export class Exp {
    constructor(lieu, date, heure_debut = new Date(), heure_fin = null, slots = [], save = true) {
        this.lieu = lieu;
        this.date = date;
        this.heure_debut = heure_debut;
        this.heure_fin = heure_fin;
        this.slots = slots;

        if (this.slots.length === 0) {
            this.create_new_slots();
        }
        this._current_slot_idx = slots.length - 1;
        if(save) this.save();
    }

    static from_string_json(json, save = true) {
        const obj = JSON.parse(json);
        return new Exp(obj.lieu, obj.date, obj.heure_debut, obj.heure_fin, obj.slots, save);
    }

    create_new_slots() {
        this.slots.push([]);
        this._current_slot_idx = this.slots.length - 1;
        this.save();
    }

    add_to_slots(status) {
        this.slots[this._current_slot_idx].push(status);
        this.save();
    }

    get_current_slot() {
        return this.slots[this._current_slot_idx];
    }

    save() {
        const date = new Date();
        this.heure_fin = `${date.getHours()}h${date.getMinutes()}`;
        window.localStorage.setItem('exp', this.toJson());
    }

    toJson() {
        const {_current_slot_idx, ...serializable} = this
        return JSON.stringify(serializable);
    }

}