import { writable } from "svelte/store";
import { Exp } from "../utils/Exp";

// TODO: SERIALIZABLE Exp CLASS 
const get_ls = window.localStorage.getItem('exp');
const exp = get_ls ? Exp.from_string_json(get_ls) : null;
export const curr_res = writable(get_ls ? Exp.from_string_json(get_ls) : null);
console.log()
export const curr_slot = writable(exp ? exp.get_current_slot(): []);

export const load_res = writable(null);