// Creating store for router.
//TODO: use the current local to find.
import Main from '../view/Main.svelte';
import NotFound from '../view/NotFound.svelte';
import CreateNewExp from '../view/CreateNewExp.svelte';
import LoadExp from '../view/LoadExp.svelte';
import ResExp from '../view/ResExp.svelte';

import { derived, writable } from "svelte/store";
export const current_root = writable(window.location.pathname);

export const MAIN_ROOT = {
    regex: new RegExp(/^\/$/),
    path: '/',
    name: "Accueil",
    component: Main
}

export const CREATE_NEW_EXP = {
    regex: new RegExp(/^\/create-exp$/),
    path: "/create-exp",
    name: "Creer une experience",
    component: CreateNewExp
}

export const LOAD_EXP = {
    regex: new RegExp(/^\/load-exp$/),
    path: "/load-exp",
    name: "Charger une experience",
    component: LoadExp
}

export const RES_EXP = {
    regex: new RegExp(/^\/res-exp$/),
    path: "/res-exp",
    name: "Resultat d'une experience",
    component: ResExp
}

export const NOT_FOUND = {
    regex: new RegExp('.*'),
    path: "/not-found",
    name: "Pas trouve",
    component: NotFound
}

export const ALL_ROUTES = [MAIN_ROOT, CREATE_NEW_EXP, LOAD_EXP, RES_EXP, NOT_FOUND];

export const current_component = derived(
    current_root,
    $ask => ALL_ROUTES.find(({regex}) => {
        return regex.test($ask);
    })
)

export const change_route_to = (root) => {
    current_root.set(root.path);
    window.history.pushState({}, root.title, root.path);
}
